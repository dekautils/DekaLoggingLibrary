# DekaLoggingLibrary

Library for log messages to files, stdout and others outputs.

## Usage

You can find instructions and examples on following links:
1. [Basic library usage](docs/basic_usage.md)

## Licence
The code for DekaLoggingLibrary is licensed under the [GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0), which can be found in full in [/LICENSE](/LICENSE).

## Building

### Prerequirements

* Compiler
* [Git](https://git-scm.com)
* [CMake(3.11+)](https://cmake.org)
* [Boost headers and filesystem module(1.67+)](https://www.boost.org)

### Getting source
You can download latest sources in zip archive using [GitLab](https://gitlab.com/dekautils/DekaLoggingLibrary/-/archive/master/DekaLoggingLibrary-master.zip) then just unzip wherever you want.

### Building with cmake
For simple building you can use these commands:
```bash
cmake ${DIR_TO_SOURCES}
cmake --build ${DIR_TO_BUILD}
```
Replace `${DIR_TO_SOURCES}` and `${DIR_TO_BUILD_DIR}` with corresponding  directories.

### Installing
This project allows you to create CPack packages and install to specified directory.  
#### CPack
To use cpack you must choose generator. Find allowed generators using `cpack --help`. And then use `cpack -G ${generator} ${DIR_TO_BUILD}`

#### Install target
First of all, choose destination: `cmake -DCMAKE_INSTALL_PREFIX=${INSTALL_PATH} ${DIR_TO_SOURCES}`. Now build with target install `cmake --build ${DIR_TO_BUILD} --target install`
