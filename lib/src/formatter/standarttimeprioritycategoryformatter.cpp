/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "standarttimeprioritycategoryformatter.hpp"
#include <ctime>
#include <iomanip>
#include <sstream>

namespace DekaLoggingLibrary::Formatter {

String StandartTimePriorityCategoryFormatter::getMessage() const
{
    using ClockType = DekaLoggingLibrary::LogMessageInfo::ClockType;
    auto info = msg->getMessageInfo();

    StringStream stream;
    auto time = ClockType::to_time_t(info.getTime());

    stream << std::put_time(localtime(&time), "%T");
    stream << " [" << info.getPriority().getName() << "]";
    stream << " [" << info.getCategory().getName() << "] ";
    stream << LogMessageFormatter::getMessage();

    return stream.str();
}

}
