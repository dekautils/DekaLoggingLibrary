/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "dekalogginglibrary_export.hpp"
#include "logmessageinfo.hpp"
#include "typedefs.hpp"
#include <memory>

namespace DekaLoggingLibrary {

class ILogMessage;
using ILogMessagePtr = std::shared_ptr<ILogMessage>;

/**
 * @brief The ILogMessage class is interface for messages.
 * @remark You shouldn't use this for logging. Use LogMessage class instead.
 */
class DEKALOGGINGLIBRARY_EXPORT ILogMessage {
public:
    ILogMessage() = default;
    virtual ~ILogMessage() = default;

    /**
     * @brief getMessage
     * @return string with message to log
     */
    virtual String getMessage() const = 0;

    /**
     * @brief getMessageInfo
     * @return message information
     */
    virtual LogMessageInfo getMessageInfo() const = 0;
};

}
