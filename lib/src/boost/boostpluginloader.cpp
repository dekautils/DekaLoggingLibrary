/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "boostpluginloader.hpp"

#include <boost/function.hpp>

namespace DekaLoggingLibrary::Boost {

ILogTargetPtr BoostPluginLoader::loadTarget(String path)
{
    boost::filesystem::path libraryPath = path;
    dll::shared_library lib;
    try {
        lib.load(libraryPath, dll::load_mode::append_decorations);
    } catch (...) {
        return nullptr;
    }

    typedef ILogTargetPtr(createTargetType)();

    boost::function<createTargetType> creator = dll::import_alias<createTargetType>(lib, "target");

    return creator();
}

}
