/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "logmessageformatter.hpp"

namespace DekaLoggingLibrary {

LogMessageFormatter::LogMessageFormatter()
    : msg(nullptr)
{
}

LogMessageFormatter::~LogMessageFormatter()
{
}

void LogMessageFormatter::setMsg(const ILogMessagePtr& value)
{
    msg = value;
}

String LogMessageFormatter::getMessage() const
{
    if (msg) {
        return msg->getMessage();
    }
    return "";
}

LogMessageInfo LogMessageFormatter::getMessageInfo() const
{
    if (msg) {
        return msg->getMessageInfo();
    }
    return LogMessageInfo();
}

}
