/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "dekalogginglibrary_export.hpp"
#include "logmessage.hpp"
#include "logmessageformatter.hpp"
#include <memory>

namespace DekaLoggingLibrary {

class ILogTarget;
using ILogTargetPtr = std::shared_ptr<ILogTarget>;

/**
 * @brief The ILogTarget class is log target interface
 * @details When connected to Log instance will recieve messages from it using ILogTarget::output() method
 */
class DEKALOGGINGLIBRARY_EXPORT ILogTarget {
public:
    ILogTarget() = default;
    virtual ~ILogTarget() = default;

    /**
     * @brief output passes message to target
     * @param msg
     */
    virtual void output(LogMessagePtr msg) = 0;
    /**
     * @brief setFormatter sets formatter to messages
     * @param formatter new formatter for your target
     */
    virtual void setFormatter(LogMessageFormatterPtr formatter) = 0;

    /**
     * @brief getName
     * @return name of target
     */
    virtual String getName() const = 0;
};

}
