/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "dekalogginglibrary_export.hpp"
#include "ilogtarget.hpp"
#include "logmessage.hpp"
#include <condition_variable>
#include <list>
#include <mutex>
#include <queue>
#include <thread>

namespace DekaLoggingLibrary {

/**
 * @brief The Log class main logging class
 * @details creates message for fill with Log::newMessage method, delivers this message when it is finished to connected ILogTarget's
 * @remark When log class created it's run Log::mainLoop async
 */
class DEKALOGGINGLIBRARY_EXPORT Log {
private:
    std::list<ILogTargetPtr> targets; ///< Connected targets

    std::queue<LogMessagePtr> queue; ///< Queue for messages
    std::mutex mutexQueue; ///< Mutex for protect queue
    std::condition_variable condVarQueue; ///< Conditional variable to make locking efficient

    std::shared_ptr<std::thread> thread; ///< Thread

    bool running;

public:
    Log();
    ~Log();

    /**
     * @brief newMessage creates message to fill
     * @return message
     * @remark You cannot reuse this message after it's finished.
     * @remark Reference can be invalid after the message finished.
     */
    LogMessage& newMessage();

    /**
     * @brief addTarget connects targets to output
     * @param target target to add
     */
    void addTarget(ILogTargetPtr target);

private:
    void mainLoop();
    void pollMessageAndTranslate();
    void translateMsgToTargets(LogMessagePtr msg);

    //Queue operations
    void push(LogMessagePtr ptr);
    LogMessagePtr top();
    void pop();
    bool empty();

    //Singleton
private:
    static Log* instance; ///< Default is nullptr

public:
    /**
     * @brief InitGlobal init singleton
     */
    static void InitGlobal();
    /**
     * @brief getInstance
     * @return singleton instance
     */
    static Log& getInstance();
    /**
     * @brief DestroyGlobal destroys singleton
     */
    static void DestroyGlobal();
};

}
