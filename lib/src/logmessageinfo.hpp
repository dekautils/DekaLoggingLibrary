/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "dekalogginglibrary_export.hpp"
#include "logcategory.hpp"
#include "logpriority.hpp"
#include "typedefs.hpp"
#include <chrono>

namespace DekaLoggingLibrary {

/**
 * @brief The LogMessageInfo class holds information about message
 */
class DEKALOGGINGLIBRARY_EXPORT LogMessageInfo {
public:
    using ClockType = std::chrono::system_clock;
    using TimePointType = ClockType::time_point;

private:
    LogPriority priority;
    LogCategory category;

    TimePointType time;

public:
    LogMessageInfo();
    ~LogMessageInfo() = default;

    /**
     * @brief getPriority
     * @return priority of message
     */
    LogPriority getPriority() const;
    /**
     * @brief setPriority sets new priority
     * @param value
     */
    void setPriority(const LogPriority& value);

    /**
     * @brief getCategory
     * @return message category
     */
    LogCategory getCategory() const;
    /**
     * @brief setCategory sets new category
     * @param value
     */
    void setCategory(const LogCategory& value);

    /**
     * @brief getTime
     * @return time when message created
     */
    TimePointType getTime() const;
};

}
