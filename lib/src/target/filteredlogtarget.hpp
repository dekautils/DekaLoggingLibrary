/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "ilogtarget.hpp"
#include <functional>

namespace DekaLoggingLibrary::Target {

class FilteredLogTarget : public ILogTarget {
public:
    using FilterFunc = std::function<bool(LogMessageInfo)>;

private:
    ILogTargetPtr target;
    FilterFunc filterFunc;

public:
    FilteredLogTarget();
    FilteredLogTarget(ILogTargetPtr toFilterTarget);
    FilteredLogTarget(ILogTargetPtr toFilterTarget, FilterFunc filter);

    ~FilteredLogTarget() override = default;

    void setTarget(const ILogTargetPtr& value);
    void setFilterFunc(const FilterFunc& value);

    // ILogTarget interface
public:
    void output(LogMessagePtr msg) override;
    void setFormatter(LogMessageFormatterPtr formatter) override;
    String getName() const override;
};

}
