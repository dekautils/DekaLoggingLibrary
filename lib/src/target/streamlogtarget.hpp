/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "../ilogtarget.hpp"
#include <iostream>
#include <memory>

namespace DekaLoggingLibrary::Target {

class StreamLogTarget : public ILogTarget {
public:
    using StreamType = std::basic_ostream<Char>;
    using StreamPtr = std::shared_ptr<StreamType>;

private:
    StreamPtr outputStream;
    LogMessageFormatterPtr formatter;

public:
    StreamLogTarget(StreamPtr outStream);
    ~StreamLogTarget() override = default;

    // ILogTarget interface
public:
    void output(LogMessagePtr msg) override;
    void setFormatter(LogMessageFormatterPtr new_formatter) override;
    String getName() const override;
};

}
