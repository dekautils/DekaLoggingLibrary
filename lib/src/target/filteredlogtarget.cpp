/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "filteredlogtarget.hpp"

namespace DekaLoggingLibrary::Target {

FilteredLogTarget::FilteredLogTarget()
    : FilteredLogTarget(nullptr, [](LogMessageInfo) -> bool { return true; })
{
}

FilteredLogTarget::FilteredLogTarget(ILogTargetPtr toFilterTarget)
    : FilteredLogTarget(toFilterTarget, [](LogMessageInfo) -> bool { return true; })
{
}

FilteredLogTarget::FilteredLogTarget(ILogTargetPtr toFilterTarget, FilteredLogTarget::FilterFunc filter)
{
    setTarget(toFilterTarget);
    setFilterFunc(filter);
}

void FilteredLogTarget::setFilterFunc(const FilterFunc& value)
{
    filterFunc = value;
}

void FilteredLogTarget::output(LogMessagePtr msg)
{
    bool filtered = filterFunc(msg->getMessageInfo());
    if (filtered && target) {
        target->output(msg);
    }
}

void FilteredLogTarget::setFormatter(LogMessageFormatterPtr formatter)
{
    if (target) {
        target->setFormatter(formatter);
    }
}

String FilteredLogTarget::getName() const
{
    if (!target) {
        return "Empty filterred target";
    }
    return "Filterred " + target->getName();
}

void FilteredLogTarget::setTarget(const ILogTargetPtr& value)
{
    target = value;
}

}
