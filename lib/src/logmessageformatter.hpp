/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "dekalogginglibrary_export.hpp"
#include "ilogmessage.hpp"
#include <memory>

namespace DekaLoggingLibrary {

class LogMessageFormatter;
using LogMessageFormatterPtr = std::shared_ptr<LogMessageFormatter>;

/**
 * @brief The LogMessageFormatter class formats message
 * @remark It is decorator class
 */
class DEKALOGGINGLIBRARY_EXPORT LogMessageFormatter : public ILogMessage {
protected:
    ILogMessagePtr msg;

public:
    LogMessageFormatter();
    ~LogMessageFormatter() override;

    /**
     * @brief setMsg sets message to decorate
     * @param value new message to decorate
     */
    void setMsg(const ILogMessagePtr& value);

    // ILogMessage interface
public:
    String getMessage() const override;
    LogMessageInfo getMessageInfo() const override;
};

}
