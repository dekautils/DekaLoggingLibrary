#pragma once

#include <sstream>
#include <string>

namespace DekaLoggingLibrary {

using Char = char;
using String = std::basic_string<Char>;
using StringStream = std::basic_ostringstream<Char>;

}
