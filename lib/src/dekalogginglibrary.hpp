/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "log.hpp"
#include "logcategory.hpp"
#include "logmessage.hpp"
#include "logmessageformatter.hpp"
#include "logmessageinfo.hpp"
#include "logpriority.hpp"

#define LOGINIT() DekaLoggingLibrary::Log::InitGlobal()
#define LOGMSG DekaLoggingLibrary::Log::getInstance().newMessage()
#define LOGADDTRGT(trgt) DekaLoggingLibrary::Log::getInstance().addTarget(trgt)
#define LOGDESTROY() DekaLoggingLibrary::Log::DestroyGlobal()
#define LOGEND DekaLoggingLibrary::LogEndMark()
