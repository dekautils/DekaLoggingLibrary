# DekaLoggingLibrary basic usage

For log messages you need to create `Log` class instance. If you need control `Log` instance lifetime, you can create it with constructor. Otherwise, you can init singleton:
```cpp
    #include <dekalogginglibrary.hpp>

    using namespace DekaLoggingLibrary;
    Log::InitGlobal();
```
or you can use define
```cpp
    LOGINIT();
```

After init to get access to you Log instance use:
```cpp
    Log::getInstance();
```

Now we should add log targets to the `Log`. We want that messages really prints to somewhere. DekaLoggingLibrary haves log target `StdOutLogTarget` that prints messages to StdOut. 
```cpp
    #include <memory>
    #include <target/stdoutlogtarget.hpp> // StdOutLogTarget not include in standart header
    ...

    auto target = std::make_shared<StdOutLogTarget>();
    Log::getInstance().addTarget(target);
    // Or you can use
    LOGADDTRGT(target)
```

Ready. How print messages? It is simple. `Log` haves method `newMessage()`. The method returns reference to `LogMessage`. It's support operations from STL library with operator `<<`.  
When you pushed all information that want, you need to push to `LogMessage` a `LogEndMark`. Result can be:
```cpp
    std::string errorMsg = "Error";
    Log::getInstance().newMessage() << errorMsg << LogEndMark();
    // Or you can use
    LOGMSG << errorMsg << LogEndMark();
```
Be careful, you must not use message that return `Log::newMessage()` after pushed end mark to it. It can cause undefined behavior. However, you **MUST** push this mark when finished. Otherwise, logging can stuck.

You probaly need to set log message priority and category. To set them, use operator `<<` with object `LogPriority` and `LogCategory` like this:
```cpp
    std::string errorMsg = "Error";
    auto& msg = Log::getInstance().newMessage();
    msg << LogPriority("CRITICAL", 0); ///< Sets priority to Critical, 0 - it is number of priority. Less - more prioritized.
    msg << LogCategory("Application"); ///< Sets category to Application
    msg << errorMsg << LogEndMark();
    //msg cannot be used here as end mark set.
```

Are you set category but it isn't prints? For printing categories, priorities, time of `LogMessage` is resposible `LogMessageFormatter`. 

When you program is going exit you should destroy `Log` instance. To destroy singleton use one of following:
* `Log::DestroyGlobal()`
* `LOGDESTROY()`
