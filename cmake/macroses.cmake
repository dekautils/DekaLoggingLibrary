macro(add_gtest)
    set(Optional )
    set(OneValue NAME)
    set(MultiValue SOURCES LIBRARIES)
    cmake_parse_arguments(
        ADD_GTEST
        "${Optional}"
        "${OneValue}"
        "${MultiValue}"
        ${ARGN})

    message(STATUS "Adding test: Name is ${ADD_GTEST_NAME}; Sources are ${ADD_GTEST_SOURCES}; Libs are ${ADD_GTEST_LIBRARIES}")
    add_executable(${ADD_GTEST_NAME} ${ADD_GTEST_SOURCES})
    target_link_libraries(${ADD_GTEST_NAME} PRIVATE gtest gtest_main gmock ${ADD_GTEST_LIBRARIES})

    gtest_discover_tests(${ADD_GTEST_NAME})
endmacro(add_gtest)
