macro(find_boost)

    find_package(Boost 1.67.0 REQUIRED COMPONENTS filesystem)

endmacro(find_boost)
